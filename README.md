**Instructions for installment**
**1** 

Download mySQL for your computer and connect to the software (to create the first account, use this link: https://www.linode.com/docs/databases/mysql/install-mysql-on-ubuntu-14-04).

`mysql -u root -p`

**2** 

Create a database with a table with the following script:

`CREATE TABLE sal_database;`


`CREATE TABLE users (
   id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
   username VARCHAR(200) NOT NULL,
   password VARCHAR(200) NOT NULL,
   isLogged BOOLEAN NOT NULL,
   isLocked BOOLEAN NOT NULL,
   isAdmin BOOLEAN NOT NULL
  );
  `
  
  `CREATE TABLE resource (
  	id_resource SMALLINT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  	name VARCHAR(200) NOT NULL,
  	description VARCHAR(200) NOT NULL
  	) ENGINE=InnoDB; `
  
  `CREATE TABLE capability (
  	id_capability SMALLINT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  	key_cap VARCHAR(200) NOT NULL,
  	ops VARCHAR(50) NOT NULL,
  	id_resource SMALLINT(6) UNSIGNED NOT NULL,
  	FOREIGN KEY(id_resource) REFERENCES resource(id_resource)
  	ON UPDATE CASCADE
  	ON DELETE CASCADE
  	) ENGINE=InnoDB;`
  
  `CREATE TABLE cap_access_control (	
  	id_capability INT,
  	id INT,
  	PRIMARY KEY(id_capability, id)
  	);`
  
  `INSERT INTO resource VALUES ('1', 
  	'teste.exe', 
  	'something for test');`
  
  `INSERT INTO resource VALUES ('1', 
  	'second_file.exe', 
  	'EMPTY');`
  
  
  
  	`INSERT INTO capability VALUES('1', 
  	'17f3e16709b943ab07a8445e1dec49ce',
  	'RWM',
  	'1');`
  
  `INSERT INTO cap_access_control VALUES('1', '24');`
  
**3** 

Download the JBDC driver from here: https://dev.mysql.com/downloads/connector/j/

**4** 

Configure Tomcat (right version: 8.5.3 -- the 9.x version is still in beta: [DOWNLOAD] https://tomcat.apache.org/download-80.cgi#8.5.23).<br>


For InteliJ: https://www.jetbrains.com/help/idea/run-debug-configuration-tomcat-server.html


For Eclipse: http://www.coreservlets.com/Apache-Tomcat-Tutorial/tomcat-7-with-eclipse.html

**5** 

Create some admin user first. Later on, create a simple user.

This creations are needed to take the full experience on the app.

Next, put the **artifact** in the deployment settings -- check links of IntelliJ/Eclipse, upwards.

Run `mvn package` followed by `mvn clean install`.

**ALL SET!**

Run the server. The link should be localhost:8081. Test with some operations.
