package servlet;

public class Resource {
    private int idResource;
    private String name;
    private String description;

    public Resource(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Resource(String name) {
        this.name = name;
    }

    public Resource(int idResource, String name, String description) {
        this.name = name;
        this.description = description;
        this.idResource = idResource;
    }

    public Resource() {
        super();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void editDescription(String newDesc) {
        description = newDesc;
    }

    public void removeDescription() {
        description = "EMPTY";
    }

    public int getIdResource() {
        return idResource;
    }
}
