package servlet;

public class Operation {
    private String operation;

    public Operation() {}

    public Operation(String operation) {this.operation=operation;}

    public String getOperation() {
        return operation;
    }
}
