package servlet;

public class Capability {
    private int idCapability;
    private String keyCapability;
    private Operation op;
    private int idResource;
    // dont forget the time

    public Capability(int idCapability,
                      String keyCapability,
                      Operation op,
                      int idResource) {
        this.idCapability = idCapability;
        this.keyCapability = keyCapability;
        this.op = op;
        this.idResource = idResource;
    }

    public Capability(String keyCapability, Operation op, int idResource) {
        this.keyCapability = keyCapability;
        this.op = op;
        this.idResource = idResource;
    }

    public Capability(String keyCapability, Operation op) {
        this.keyCapability = keyCapability;
        this.op = op;
    }

    public int getIdCapability() {
        return idCapability;
    }

    public String getKeyCapability() {
        return keyCapability;
    }

    public Operation getOp() {
        return op;
    }

    public int getIdResource() {
        return idResource;
    }

}
