package servlet;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccessController {
    public AccessController() {
        super();
    }

    public Capability makeKey(Resource res, Operation op) {
        String fillToEnc = res.getName();
        CrypService cs = new CrypService();
        String rslt = "";
        rslt = cs.doEncryption(fillToEnc);
        Capability capRst = new Capability(rslt, op);
        return capRst;
    }

    public boolean checkPermissions(Capability cap, Resource res, Operation op) {
        Authenticator as = new Authenticator();
        Connection con = null;
        DataSource ds = as.getDS();
        try {
            con = ds.getConnection();
            String query = "SELECT * FROM capability WHERE key_cap = ?";
            PreparedStatement preparedStmt = con.prepareStatement(query);
            preparedStmt.setString(1, cap.getKeyCapability());
            ResultSet rs = preparedStmt.executeQuery();
            int rsFetchSize = rs.getFetchSize();

            String query_ = "SELECT * FROM resources WHERE id_resource = ?";
            PreparedStatement preparedStmt_ = con.prepareStatement(query_);
            preparedStmt_.setInt(1, res.getIdResource());
            ResultSet rs_ = preparedStmt.executeQuery();
            int rsFetchSize_ = rs_.getFetchSize();

            if(rsFetchSize == 0 || rsFetchSize_ == 0) {
                return false;
            } else if(rsFetchSize >= 1 && rsFetchSize_ == 1) {
                while(rs.next()) {
                    String ops = rs.getString("ops");
                    if(ops.contains(op.getOperation())) {
                        return true;
                    }
                }
            }
        } catch (SQLException e) {
            e.getSQLState();
        }
        return false;
    }
}
