<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.Account" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.PreparedStatement" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 27-12-2017
  Time: 8:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Description</title>
    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
<%
    HttpSession hs = request.getSession(true);
    String aux = hs.getAttribute("USER").toString();
    Authenticator as = new Authenticator();
    Account acc = as.getAccount(aux);
    Connection con = null;
    DataSource ds = as.getDS();
    con = ds.getConnection();
    String nameEdit = request.getParameter("editName");
    String newDescription = request.getParameter("editDescriptionName");
    String updateQuery = "UPDATE resource SET description = ? WHERE name = ?";
    PreparedStatement prepS;
    prepS = con.prepareStatement(updateQuery);
    prepS.setString(1, newDescription);
    prepS.setString(2, nameEdit);
    prepS.execute();
    con.close();

%>

<div class="jumbotron">
    <div class="container"  style="margin-top:30px; font-family: 'Muli', sans-serif;">
        <a href="/home.jsp" class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-home"></span> Home</a>
        <p>Edit done.</p>
    </div></div>
</body>
</html>
