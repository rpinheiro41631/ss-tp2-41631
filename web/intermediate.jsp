<%@ page import="servlet.Authenticator" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 23:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Middle page</title>

    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
<div class="jumbotron">
    <div class="container" style="margin-top:30px; font-family: 'Muli', sans-serif;">

    </div>
</div>

    <%
        Authenticator as = new Authenticator();
        String username = request.getParameter("username");
        as.deleteAccout(username);
        response.sendRedirect("/home.jsp");
    %>
</body>
</html>
