<%@ page import="servlet.*" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 27-12-2017
  Time: 6:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Permissions Granted</title>
    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
<div class="container" style="margin-top:30px; font-family: 'Muli', sans-serif;">
    <div class="jumbotron">
        <p style="font-family: 'Muli', sans-serif;">Permissions granted.</p><br>
        <a href="/home.jsp" class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-home"></span> Home
        </a>
    </div>
</div>

<%
    PreparedStatement preparedStatementF, preparedStatementC,
            preparedStatementU, preparedStatementX, preparedStatementZ;
    int idResource = 0;
    int idUser = 0;
    int idCapability = 0;

    String opValue = request.getParameter("opValue");
    String selUser = request.getParameter("selectUser");
    String selRes = request.getParameter("selectRes");

    Resource res = new Resource(selRes);
    Operation op = new Operation(opValue);

    AccessController ac = new AccessController();
    Capability cap = ac.makeKey(res, op);

    Authenticator as = new Authenticator();
    DataSource dataSour = as.getDS();
    Connection con;
    con = dataSour.getConnection();

    String findResIdQuery = "SELECT * FROM resource WHERE name = ?;";
    preparedStatementF = con.prepareStatement(findResIdQuery);
    preparedStatementF.setString(1, selRes);
    ResultSet rs = preparedStatementF.executeQuery();
    while(rs.next()) {
        idResource = rs.getInt("id_resource");
    }

    String execCapQuery = "INSERT INTO capability VALUES(NULL, ?, ?, ?);";
    preparedStatementC = con.prepareStatement(execCapQuery);
    preparedStatementC.setString(1, cap.getKeyCapability());
    preparedStatementC.setString(2, cap.getOp().getOperation());
    preparedStatementC.setInt(3, idResource);
    preparedStatementC.execute();

    String findUserIdQuery = "SELECT * FROM users WHERE username = ?;";
    preparedStatementU = con.prepareStatement(findUserIdQuery);
    preparedStatementU.setString(1, selUser);
    ResultSet rsltSetUser = preparedStatementU.executeQuery();
    while(rsltSetUser.next()) {
        idUser = rsltSetUser.getInt("id");
    }

    String findIdCapability = "SELECT * FROM capability WHERE key_cap = ?;";
    preparedStatementX = con.prepareStatement(findIdCapability);
    preparedStatementX.setString(1, cap.getKeyCapability());
    ResultSet rsltSetCap = preparedStatementX.executeQuery();
    while(rsltSetCap.next()) {
        idCapability = rsltSetCap.getInt("id_capability");
    }

    String execCapAccess = "INSERT INTO cap_access_control VALUES(?, ?);";
    preparedStatementZ = con.prepareStatement(execCapAccess);
    preparedStatementZ.setInt(1, idCapability);
    preparedStatementZ.setInt(2, idUser);
    preparedStatementZ.execute();
    con.close();

%>
</body>
</html>
