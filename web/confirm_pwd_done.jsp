<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.CrypService" %>
<%@ page import="servlet.Account" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 3:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>New password confirmed</title>
    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
    <div class="container">
        <div class="jumbotron" style="margin-top:30px; font-family: 'Muli', sans-serif;">
            <p style="font-family: 'Muli', sans-serif;">Password changed.</p>
            <a href="/home.jsp" class="btn btn-default btn-sm">
                <span class="glyphicon glyphicon-home"></span> Home
             </button>
        </div>
    </div>
    <%
        String newPwd = request.getParameter("password");
        String newPwdConf = request.getParameter("password1");
        CrypService cp = new CrypService();
        Authenticator auth = new Authenticator();
        String aux = cp.doEncryption(newPwd);
        HttpSession hs = request.getSession(true);
        String userTmp = hs.getAttribute("USER").toString();
        Account acc = auth.getAccount(userTmp);
        if(newPwd.equals(newPwdConf)) {
            auth.changePassword(userTmp, aux, aux);
        } else {
            response.sendRedirect("/error.jsp");
        }
    %>
</body>
</html>
