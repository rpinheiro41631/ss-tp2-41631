<%@ page import="servlet.Authenticator" %>
<%@ page import="servlet.Account" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="servlet.AccessController" %>
<%@ page import="javax.xml.transform.Result" %><%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 27-12-2017
  Time: 3:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Resource Explorer</title>

    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
<div class="jumbotron">
    <div class="container"  style="margin-top:30px; font-family: 'Muli', sans-serif;">
        <a href="/home.jsp" class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-home"></span> Home</a>
        <%
            HttpSession hs = request.getSession(true);
            String aux = hs.getAttribute("USER").toString();
            Authenticator as = new Authenticator();
            Account acc = as.getAccount(aux);

            DataSource dataSour = as.getDS();
            Connection con;
            con = dataSour.getConnection();

            if(acc.getAdminStatus()) {
                String query = "SELECT * FROM resource;";
                PreparedStatement preparedStmt = con.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                out.println("<table class=\"table table-bordered\">");out.println("<thead>");
                out.println("<tr>");
                out.println("<th>Name</th>");
                out.println("<th>Description</th>");
                out.println("</tr>");
                out.println("</thead>");
                out.println("<tbody>");
                while(rs.next()) {
                    String name = rs.getString("name");
                    String desc = rs.getString("description");
                    out.println("<tr>");
                    out.println("<td>");
                    out.println(name);
                    out.println("</td>");
                    out.println("<td>");
                    out.println(desc);
                    out.println("</td>");
                    out.println("</tr>");
                }
                out.println("</tbody>");
                out.println("</table>");
                out.println("<br>");

                out.println("<form action=\"/edit_desc.jsp\" method=\"POST\">");
                out.println("<input type=\"text\" class=\"btn btn-warning\" value=\"Resource Name to Edit\" name=\"editName\"></input>");
                out.println("<input type=\"text\" class=\"btn btn-warning\" value=\"New Description\" name=\"editDescriptionName\"></input>");
                out.println("</form>");
                out.println("<form action=\"/remove_desc.jsp\" method=\"POST\">");
                out.println("<input type=\"text\" class=\"btn btn-danger\" value=\"Resource Name to Delete Description\" name=\"removeName\"></input>");
                out.println("</form>");

                out.println("<h1>Give permissions</h1>");
                String query_ = "SELECT * FROM users;";
                PreparedStatement preparedStmt_ = con.prepareStatement(query_);
                ResultSet rs_ = preparedStmt_.executeQuery();
                out.println("<form action=\"/permGive.jsp\" method=\"POST\">");
                out.println("Select user to give permissions:");
                out.println("<br>");
                out.println("<select name=\"selectUser\" size=\"5\">");
                while(rs_.next()) {
                    out.println("<option>");
                    String userN = rs_.getString("username");
                    out.println(userN);
                    out.println("</option>");
                }
                out.println("</select>");

                String q = "SELECT * FROM resource;";
                PreparedStatement prp = con.prepareStatement(q);
                ResultSet rsltSet = prp.executeQuery();
                out.println("Select the resource:");
                out.println("<br>");
                out.println("<select name=\"selectRes\" size=\"5\">");
                while(rsltSet.next()) {
                    out.println("<option>");
                    String rsc = rsltSet.getString("name");
                    out.println(rsc);
                    out.println("</option>");
                }
                out.println("<input type=\"text\" name=\"opValue\" value=\"Read | Write | Delete\">");
                out.println("<br>");
                out.println("<input type=\"submit\"></input>");
                out.println("</form>");
            } else {
                PreparedStatement prepA, prepB;
                int idUser = 0;

                String fIdUser = "SELECT * FROM users WHERE username = ?";
                prepB = con.prepareStatement(fIdUser);
                prepB.setString(1, acc.getUsername());
                ResultSet resultUser = prepB.executeQuery();
                while (resultUser.next()) {
                    idUser = resultUser.getInt("id");
                }

                String filesKnownToUserQuery = "SELECT DISTINCT name, description " +
                        "FROM cap_access_control NATURAL JOIN capability " +
                        " NATURAL JOIN resource WHERE id = ?";
                prepA = con.prepareStatement(filesKnownToUserQuery);
                prepA.setInt(1, idUser);
                ResultSet allFiles = prepA.executeQuery();
                out.println("<table class=\"table table-bordered\">");
                out.println("<thead>");
                out.println("<tr>");
                out.println("<th>Name</th>");
                out.println("<th>Description</th>");
                out.println("</tr>");
                out.println("</thead>");
                out.println("<tbody>");
                while (allFiles.next()) {
                    String name = allFiles.getString("name");
                    String desc = allFiles.getString("description");
                    out.println("<tr>");
                    out.println("<td>");
                    out.println(name);
                    out.println("</td>");
                    out.println("<td>");
                    out.println(desc);
                    out.println("</td>");
                    out.println("</tr>");
                }
                out.println("</tbody>");
                out.println("</table>");
                out.println("<br>");

                out.println("<form action=\"/edit_desc.jsp\" method=\"POST\">");
                out.println("<input type=\"text\" class=\"btn btn-warning\" value=\"Resource Name to Edit\" name=\"editName\"></input>");
                out.println("<input type=\"text\" class=\"btn btn-warning\" value=\"New Description\" name=\"editDescriptionName\"></input>");
                out.println("<input type=\"submit\" class=\"btn\" value=\"Finish Edit\">");
                out.println("</form>");
                out.println("<form action=\"/remove_desc.jsp\" method=\"POST\">");
                out.println("<input type=\"text\" class=\"btn btn-danger\" value=\"Resource Name to Delete Description\" name=\"removeName\"></input>");
                out.println("</form>");
            }
            con.close();

            // delete, read, write buttons (check perm)
        %>
    </div>
</div>
</body>
</html>
