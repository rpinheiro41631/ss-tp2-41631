<%--
  Created by IntelliJ IDEA.
  User: rp
  Date: 19-11-2017
  Time: 2:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Change Password</title>

    <!-- Begin imports -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- End imports -->

    <meta charset="utf-8">

    <!-- Begin mobile friendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- End mobile friendly -->

    <!-- Begin font import -->
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <!-- End font import -->
</head>
<body>
    <div class="container">
        <div class="jumbotron" style="margin-top:30px; font-family: 'Muli', sans-serif;">
            <form name="changePwd" action="/confirm_pwd_done.jsp" method="POST">
                <div class="form-group">
                    <label for="pwd1">New password:</label>
                    <input type="password" id="pwd1" name="password">
                </div>
                <div class="form-group">
                    <label for="pwd2">Confirm it:</label>
                    <input type="password" id="pwd2" name="password1">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
                <input type="hidden" value=redirect_url>
            </form>
        </div>
    </div>
</body>
</html>
